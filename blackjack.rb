#!/usr/bin/env ruby
def deal(cards)
  cards.each do |card|
    print "[#{card['suit']} #{card['rank']} ]"
  end
  puts
end


def first_deal_dealer(points_rank, dealer_cards)
  print "( #{points_rank[dealer_cards[1]['rank']]} ):[ ?? ][#{dealer_cards[1]['suit']} #{dealer_cards[1]['rank']} ]"
  puts 
end

def sum_points(player_points)
  sum = player_points.sum
  if sum > 21
    if player_points.count(11) != 0
      if player_points.count(11) * 10 >= sum - 21
        while sum > 21
          sum = sum - 10
        end
      end
    end
  end
  return sum
end

def player_enters_name
  puts "enter player's name"
  input = STDIN.gets.chomp
  input !='' ? input : false
end

def money_left(players, dollars, num_of_players)
  num_of_players.times do |i|
    puts "#{players[i]} has #{dollars[i]} left"
  end
end

def player_enters_bet(num_of_players, players, bets, dollars, all_cards_points)
  num_of_players.times do |i|
    if dollars[i] < 10
      puts " #{players[i]} you have no money to continue"
      bets << 0
      all_cards_points[i] = [0,0]
    else
      puts " #{players[i]} enter bet amount, min 10$"
      input = STDIN.gets.chomp.to_i
      while input < 10
        puts 'min bet amount is 10 $'
        input = STDIN.gets.chomp.to_i
      end
      while input > dollars[i]
        puts " you can bet max #{dollars[i]}"
        input = STDIN.gets.chomp.to_i
      end
      bets << input
      dollars[i] = dollars[i] - input
    end
  end
end

new_deck = [
  {'suit' => '♠', 'rank' => '2'},
  {'suit' => '♠', 'rank' => '3'},
  {'suit' => '♠', 'rank' => '4'},
  {'suit' => '♠', 'rank' => '5'},
  {'suit' => '♠', 'rank' => '6'},
  {'suit' => '♠', 'rank' => '7'},
  {'suit' => '♠', 'rank' => '8'},
  {'suit' => '♠', 'rank' => '9'},
  {'suit' => '♠', 'rank' => '10'},
  {'suit' => '♠', 'rank' => 'J'},
  {'suit' => '♠', 'rank' => 'Q'},
  {'suit' => '♠', 'rank' => 'K'},
  {'suit' => '♠', 'rank' => 'A'},

  {'suit' => '♥', 'rank' => '2'},
  {'suit' => '♥', 'rank' => '3'},
  {'suit' => '♥', 'rank' => '4'},
  {'suit' => '♥', 'rank' => '5'},
  {'suit' => '♥', 'rank' => '6'},
  {'suit' => '♥', 'rank' => '7'},
  {'suit' => '♥', 'rank' => '8'},
  {'suit' => '♥', 'rank' => '9'},
  {'suit' => '♥', 'rank' => '10'},
  {'suit' => '♥', 'rank' => 'J'},
  {'suit' => '♥', 'rank' => 'Q'},
  {'suit' => '♥', 'rank' => 'K'},
  {'suit' => '♥', 'rank' => 'A'},

  {'suit' => '♣', 'rank' => '2'},
  {'suit' => '♣', 'rank' => '3'},
  {'suit' => '♣', 'rank' => '4'},
  {'suit' => '♣', 'rank' => '5'},
  {'suit' => '♣', 'rank' => '6'},
  {'suit' => '♣', 'rank' => '7'},
  {'suit' => '♣', 'rank' => '8'},
  {'suit' => '♣', 'rank' => '9'},
  {'suit' => '♣', 'rank' => '10'},
  {'suit' => '♣', 'rank' => 'J'},
  {'suit' => '♣', 'rank' => 'Q'},
  {'suit' => '♣', 'rank' => 'K'},
  {'suit' => '♣', 'rank' => 'A'},

  {'suit' => '♦', 'rank' => '2'},
  {'suit' => '♦', 'rank' => '3'},
  {'suit' => '♦', 'rank' => '4'},
  {'suit' => '♦', 'rank' => '5'},
  {'suit' => '♦', 'rank' => '6'},
  {'suit' => '♦', 'rank' => '7'},
  {'suit' => '♦', 'rank' => '8'},
  {'suit' => '♦', 'rank' => '9'},
  {'suit' => '♦', 'rank' => '10'},
  {'suit' => '♦', 'rank' => 'J'},
  {'suit' => '♦', 'rank' => 'Q'},
  {'suit' => '♦', 'rank' => 'K'},
  {'suit' => '♦', 'rank' => 'A'},
]

deck = new_deck.clone
deck = deck * 5
deck.shuffle!

points_rank = {'2' => 2, '3' => 3, '4' => 4, '5' => 5, '6' => 6, '7' => 7, '8' => 8, '9' => 9, '10' => 10, 'J' => 10, 'Q' => 10, 'K' => 10, 'A' => 11}

dealer_cards = []

dealer_points = []

players = []

player_cards = []

player_points = []

all_cards_points = []

array_help = []

blackjack = []

final_points = []

dollars = []

bets = []

num_of_players = 0
while player = player_enters_name
  players[num_of_players] = player
  num_of_players += 1
end

num_of_players.times do
  dollars << 100
end

players << 'dealer'
dollars << 0

input = 'y'

while input == 'y'

  if deck.size <= 13
    deck = new_deck.clone
    deck = deck * 5
    deck.shuffle!
  end

  player_enters_bet(num_of_players, players, bets, dollars, all_cards_points)

  bets << 0

  num_of_players.times do |count|
    if bets[count] >= 10
      2.times do |i|
        player_cards << deck.shift
        player_points[i] = points_rank[player_cards[i]['rank']]
      end
      print "#{players[count]}: (#{dollars[count]}$ ): ( #{sum_points(player_points)} ):"
      deal(player_cards)
      array_help.push(player_cards, player_points)
      all_cards_points[count] = array_help
      array_help = []
      player_cards = []
      player_points = []
    end
  end
  
  if bets.sum >= 10
    2.times do |i|
      dealer_cards << deck.shift
      dealer_points[i] = points_rank[dealer_cards[i]['rank']]
    end

    print "dealer"
    first_deal_dealer(points_rank, dealer_cards)
    puts
  end

  num_of_players.times do |count|
    
    if bets[count] >= 10
      if sum_points(all_cards_points[count][1]) == 21
        blackjack[count] = 1
      else
        blackjack[count] = 0
      end

      if sum_points(all_cards_points[count][1]) >= 18
        print "#{players[count]}: ( #{sum_points(all_cards_points[count][1])} ):"
        deal(all_cards_points[count][0])
      else
        puts "#{players[count]}:for hit enter 'h' for stand enter 's'"
      end

      i = 2
      while sum_points(all_cards_points[count][1]) < 18
        input = STDIN.gets.chomp
        if input == 'h'
          all_cards_points[count][0] << deck.shift
          all_cards_points[count][1][i] = points_rank[all_cards_points[count][0][i]['rank']]
          print "#{players[count]}: ( #{sum_points(all_cards_points[count][1])} ):"
          deal(all_cards_points[count][0])
          i += 1
        else
          if input == 's'
            break
          else
            puts "for hit enter 'h' for stand enter 's'"
          end
        end
      end
      final_points[count] = sum_points(all_cards_points[count][1])
    else
      blackjack[count] = 0
      final_points[count] = 0
    end
  end

  if sum_points(dealer_points) > 16
    if sum_points(dealer_points) == 21
      blackjack << 1
    else
      blackjack << 0
    end
    print "dealer ( #{sum_points(dealer_points)} ):"
    deal(dealer_cards)
  end

  if final_points.each { |item| item <= 21 } and bets.sum >= 10
    i = 2
    while sum_points(dealer_points) <= 16
      dealer_cards << deck.shift
      dealer_points[i] = points_rank[dealer_cards[i]['rank']]
      print "dealer ( #{sum_points(dealer_points)} ):"
      deal(dealer_cards)
      i += 1
    end
  end

  final_points << sum_points(dealer_points)

  puts '___~~~***^o8o^***~~~___'

  points = blackjack.each_index.select { |i| blackjack[i] == 1}
  if points.any?
    points.each do |item|
      puts "#{players[item]} black jack!"
      dollars[item] += bets[item]*2
    end
  else
    points = final_points.select { |item|  item > 0 and item <= 21  }
    if points.any?
      21.downto(1) do |number_of_points|
      points = final_points.each_index.select { |i| final_points[i] == number_of_points}
        if points.any?
          points.each do |item|
            puts "#{players[item]} wins !"
            dollars[item] += bets[item] * 2
          end
          break
        end
      end
    else
      puts "everyone busted!"
    end
  end

  money_left(players, dollars, num_of_players)

  if dollars.sum >= 10
    puts '/\_/\ '
    puts ' ^ ^'
    puts "If you want to continue enter 'y'. If you want to leave the game enter any other key"
    input = STDIN.gets.chomp
  else
    input = 'no'
    puts '/\_/\ '
    puts ' # #'
  end
  puts '$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$'
  dealer_cards = []
  player_cards = []
  player_points = []
  dealer_points = []
  all_cards_points = []
  array_help = []
  blackjack = []
  final_points = []
  bets = []
end

