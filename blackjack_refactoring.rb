#!/usr/bin/env ruby
new_deck = [
  {'suit' => '♠', 'rank' => '2'},
  {'suit' => '♠', 'rank' => '3'},
  {'suit' => '♠', 'rank' => '4'},
  {'suit' => '♠', 'rank' => '5'},
  {'suit' => '♠', 'rank' => '6'},
  {'suit' => '♠', 'rank' => '7'},
  {'suit' => '♠', 'rank' => '8'},
  {'suit' => '♠', 'rank' => '9'},
  {'suit' => '♠', 'rank' => '10'},
  {'suit' => '♠', 'rank' => 'J'},
  {'suit' => '♠', 'rank' => 'Q'},
  {'suit' => '♠', 'rank' => 'K'},
  {'suit' => '♠', 'rank' => 'A'},

  {'suit' => '♥', 'rank' => '2'},
  {'suit' => '♥', 'rank' => '3'},
  {'suit' => '♥', 'rank' => '4'},
  {'suit' => '♥', 'rank' => '5'},
  {'suit' => '♥', 'rank' => '6'},
  {'suit' => '♥', 'rank' => '7'},
  {'suit' => '♥', 'rank' => '8'},
  {'suit' => '♥', 'rank' => '9'},
  {'suit' => '♥', 'rank' => '10'},
  {'suit' => '♥', 'rank' => 'J'},
  {'suit' => '♥', 'rank' => 'Q'},
  {'suit' => '♥', 'rank' => 'K'},
  {'suit' => '♥', 'rank' => 'A'},

  {'suit' => '♣', 'rank' => '2'},
  {'suit' => '♣', 'rank' => '3'},
  {'suit' => '♣', 'rank' => '4'},
  {'suit' => '♣', 'rank' => '5'},
  {'suit' => '♣', 'rank' => '6'},
  {'suit' => '♣', 'rank' => '7'},
  {'suit' => '♣', 'rank' => '8'},
  {'suit' => '♣', 'rank' => '9'},
  {'suit' => '♣', 'rank' => '10'},
  {'suit' => '♣', 'rank' => 'J'},
  {'suit' => '♣', 'rank' => 'Q'},
  {'suit' => '♣', 'rank' => 'K'},
  {'suit' => '♣', 'rank' => 'A'},

  {'suit' => '♦', 'rank' => '2'},
  {'suit' => '♦', 'rank' => '3'},
  {'suit' => '♦', 'rank' => '4'},
  {'suit' => '♦', 'rank' => '5'},
  {'suit' => '♦', 'rank' => '6'},
  {'suit' => '♦', 'rank' => '7'},
  {'suit' => '♦', 'rank' => '8'},
  {'suit' => '♦', 'rank' => '9'},
  {'suit' => '♦', 'rank' => '10'},
  {'suit' => '♦', 'rank' => 'J'},
  {'suit' => '♦', 'rank' => 'Q'},
  {'suit' => '♦', 'rank' => 'K'},
  {'suit' => '♦', 'rank' => 'A'},
]

def get_deck(template_deck)
  deck = template_deck.clone
  deck = deck * 5
  deck.shuffle!
  return deck
end

def get_player_name
  puts "enter next player's name (empty = no more players)"
  name = STDIN.gets.chomp
  if name.empty?
    return nil
  end
  return name
end

def score(hand)
  result = 0
  aces = 0
  hand.each do |card|
    if card['hidden'] == true
      return '?'
    end
    if card['rank'].to_i > 0
      result += card['rank'].to_i
    elsif card['rank'] == 'A'
      aces += 1
    else
      result += 10
    end
  end
  aces.times do
    if result > 10
      result += 1
    else
      result += 11
    end
  end
  return result
end

def print_hand(name, hand, bet = nil)
  print "#{name}("
  print score(hand).to_s
  if bet 
    print " / $#{bet}"
  end
  print '): '
  hand.each do |card|
    print '['
    if card['hidden'] == true
      print '??'
    else
      print "#{card['suit']} #{card['rank']}"
    end
    print ']'
  end
  puts 
end

def print_table(dealer_hand, player_hand = nil, player_name = nil, bet = nil)
  puts 
  puts '=' * 40
  print_hand('dealer', dealer_hand)
  if player_hand != nil && player_name != nil
    print_hand(player_name, player_hand, bet)
  end
end

def result(dealer_hand, player_hand, player_name)
  win = false
  dealer = score(dealer_hand)
  player = score(player_hand)
  if player > 21
    puts "#{player_name} loose! BUST"
  elsif player <= 21 && dealer > 21
    win = true
    puts "#{player_name} wins! dealer bust"
    if player_hand.size == 2 && player == 21
      puts 'BLACKJACK!'
    end
  elsif player == 21 && player_hand.size == 2 && !(dealer == 21 && dealer_hand.size == 2)
    win = true
    puts "#{player_name} wins! BLACKJACK!"
  elsif dealer == 21 && dealer_hand.size == 2 && !(player == 21 && player_hand.size == 2)
    puts "#{player_name} loose. Dealer blackjack"
  elsif player == dealer
    win = nil
    puts "#{player_name} it's a tie!"
  elsif player > dealer
    win = true
    puts "#{player_name} wins"
  else
    puts "#{player_name} loose"
  end
  return win
end

deck = get_deck(new_deck)

players = {}
wallets = {}
bets = {}
min_bet = 10

while name = get_player_name
  players[name] = []
  wallets[name] = 100
  bets[name] = 0
end

play = true
while play
  if deck.size < 13
    deck = get_deck(new_deck)
  end
  players.keys.each do |name|
    players[name] = []
  end
  dealer = []

  # bets
  puts "minimum bet $#{min_bet}"
  players.keys.each do |name|
    if bets[name] == 0
      if wallets[name] < min_bet
        puts "#{name} you have no money to continue"
        players.delete(name)
      else
        print "#{name} you have $#{wallets[name]}, enter your bet (0 to quit): "
        bet = STDIN.gets.chomp.to_i
        if bet >= min_bet
          if bet <= wallets[name]
            bets[name] = bet
          else
            bets[name] = wallets[name]
          end
        else
          puts "#{name} leaves the table with $#{wallets[name]}"
          players.delete(name)
        end
      end
    else
      puts "#{name} you play for previous bet - $#{bets[name]}"
    end
  end
  if players.size == 0
    puts 'All players left the table.'
    break
  end

  players.keys.each do |name|
    players[name] << deck.shift
    players[name] << deck.shift
  end
  dealer << deck.shift
  dealer << deck.shift
  dealer[0]['hidden'] = true

  # players turn
  players.keys.each do |name|
    hand = players[name]
    print_table(dealer, hand, name, bets[name])
    while score(hand) < 19
      print "enter 'h' to hit, 's' to stand: "
      response = STDIN.gets.chomp
      if response != 'h'
        break
      end
      hand << deck.shift
      print_table(dealer, hand, name, bets[name])
    end
  end

  # dealer's turn
  dealer[0]['hidden'] = nil
  print_table(dealer)
  while score(dealer) < 17
    dealer << deck.shift
    print_table(dealer)
    sleep 1
  end

  players.each do |name, hand|
    win = result(dealer, hand, name)
    if win 
      wallets[name] += bets[name]
      bets[name] = 0
    elsif win == false
      wallets[name] -= bets[name]
      bets[name] = 0
    end   
  end

  puts "Play again? [y/n]"
  response = STDIN.gets.chomp
  play = (response == 'y')

end

players.keys.each do |name|
  puts "#{name}: $#{wallets[name]}"
end